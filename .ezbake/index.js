const {
  exec,
  execSync
} = require('child_process');

console.log('. Installing required dependencies...');
execSync('yarn install', {
  cwd: __dirname
});
console.log('. Finished installing required dependencies');

const { isValidURL, formatUrl } = require('@appirio/appirio/lib/util/misc');
const config = require('@appirio/appirio/lib/config/config');

let isDifferentCiCdSystem = false;
let repoUrl = '';
let ciCdUsername = null;
let ciCdPersonalToken = null;

const storeSecret = (secretName) => {
  return (input, answers) => {
    answers._secrets = answers._secrets || {};
    answers._secrets[secretName] = input;
    return true;
  }
}

const checkIfDifferentCiCdSystem = async (continuousIntegrationURL) => {
  const ciUrl = formatUrl(continuousIntegrationURL);
  if ((repoUrl !== '' && repoUrl !== ciUrl)) {
    isDifferentCiCdSystem = true;
  }
};

const appirio = {
  gitLabUrl: 'https://gitlab.appirio.com',
  sonarQubeUrl: "https://sonarqube.appirio.com"
};

const setRepoUrl = (value) => {
  repoUrl = value;
}

module.exports = {
  setRepoUrl,
  source: {
    "**/config/*": true,
    "**/.gitlab-ci.yml": true,
    "**/.ezbake/scripts/ezbaker.js": true,
    "**/.ezbake/scripts/secrets.js": true,
    "**/sfdx-project.json": false
  },
  ingredients: [{
    type: "list",
    name: "usesCMC",
    message: "Will your project use CMC?",
    "choices": ["Yes", "No"],
    "default": "Yes",
    filter: response => (response === "Yes"),
  },
  {
    type: "list",
    name: "continuousIntegrationType",
    message: "Which CI System would you like to use?",
    choices: ["GitLab CI", "None"],
    default: "GitLab CI",
    filter: response => {
      return {
        'GitLab CI': 'gitlab',
        'None': 'none'
      }[response];
    }
  },
  {
    type: 'list',
    name: 'continuousIntegrationURLSame',
    message: () => {
      return `Is your project URL on Gitlab: ${repoUrl}?`;
    },
    choices: ['Yes', 'No'],
    default: 'Yes',
    filter: val => (val === 'Yes'),
    when: answers => {
      return answers.continuousIntegrationType !== 'none';
    },
  },
  {
    type: 'input',
    name: 'continuousIntegrationURL',
    message: `What is your project URL on Gitlab?`,
    validate: async (input) => {
      if (input === '') {
        return 'You must provide your Project URL';
      }
      if (!isValidURL(input)) {
        return 'You must provide a valid URL!';
      }
      await checkIfDifferentCiCdSystem(input);
      return true;
    },
    when: (answers) => {
      if (answers.continuousIntegrationURLSame) {
        answers.continuousIntegrationURL = repoUrl;
        return false;
      }
      return (answers.continuousIntegrationType !== 'none');
    },
  },
  {
    type: 'input',
    name: 'repo__username',
    message: 'What is your Git username?',
    default: () => {
      if (config.hasUserConfig('gitlab.username') && !isDifferentCiCdSystem) {
        ciCdUsername = config.readUserConfig('gitlab.username');
      }
      return ciCdUsername;
    },
    when: (answers) => {
      answers.repo__username = answers.repo__username || '';
      return (answers.continuousIntegrationType !== 'none');
    },
    validate: (input) => {
      if (input === '') {
        return 'You must provide your Git username';
      }
      return true;
    },
  },
  {
    type: "password",
    mask: '*',
    name: 'repo__personal_token',
    message: 'What is your Git password/access token?',
    default: () => {
      if (config.hasSecret('gitlab.personal_token') && !isDifferentCiCdSystem) {
        ciCdPersonalToken = config.getSecret('gitlab.personal_token');
      }
      return ciCdPersonalToken;
    },
    when: (answers) => {
      answers.repo__personal_token = answers.repo__personal_token || '';
      return (answers.continuousIntegrationType !== 'none');
    },
    validate: function (input) {
      if (input == '') {
        return 'You must provide your Git password/access token';
      }
      return true;
    },
  },
  {
    type: 'password',
    mask: '*',
    name: 'ci_cd__personal_token',
    message: `What is your Gitlab access token?`,
    default: () => {
      if (config.hasSecret('gitlab.personal_token') && isDifferentCiCdSystem) {
        ciCdPersonalToken = config.getSecret('gitlab.personal_token');
      }
      return ciCdPersonalToken;
    },
    when: (answers) => {
      // If we don't have different CI system than repo URL, ci_cd__personal_token and repo__personal_token are same
      answers.ci_cd__personal_token = answers.repo__personal_token || '';
      return (answers.continuousIntegrationType !== 'none') && isDifferentCiCdSystem;
    },
    validate: (input) => {
      if (input === '') {
        return `You must provide your Gitlab access token`;
      }
      return true;
    },
  },
  {
    type: "list",
    name: "enableSonarQube",
    message: "Enable quality scanning using SonarQube?",
    choices: ["Yes", "No"],
    filter: val => (val === "Yes")
  },
  {
    type: "input",
    name: "sonarUrl",
    message: "What's the URL of your SonarQube instance?",
    when: answers => {
      return answers.enableSonarQube;
    },
    default: appirio.sonarQubeUrl
  },
  {
    "type": "list",
    "name": "cleanUpBranches",
    "message": "Automatically clean up branches that have been merged?",
    when: answers => {
      answers.cleanUpBranches = answers.cleanUpBranches || '';
      return answers.continuousIntegrationType !== 'none';
    },
    choices: ["Yes", "No"],
    default: "Yes",
    filter: val => (val === "Yes")
  },
  {
    type: 'list',
    message: 'Which set of orgs will you be deploying to for your project?',
    name: 'orgs',
    choices: [{
      name: 'QA, SIT, UAT, Production',
      value: 'QA,SIT,UAT,PROD'
    },
    {
      name: 'SIT, UAT, Production',
      value: 'SIT,UAT,PROD'
    },
    {
      name: 'QA, UAT, Production',
      value: 'QA,UAT,PROD'
    },
    {
      name: 'UAT, Production',
      value: 'UAT,PROD'
    },
    {
      name: 'Production',
      value: 'PROD',
      checked: true
    }
    ],
    validate: function (answer) {
      if (answer.length < 1) {
        return 'You must choose at least one org.';
      }
      return true;
    }
  },
  {
    type: "list",
    name: "sourceBranchToClone",
    message: "Which branch do you want to create new feature branches from?",
    choices: (answers) => {
      let org_list = answers.orgs.split(',');
      return (org_list.length >= 3) ? [org_list[0], "master"] : ["master"];
    },
    default: (answers) => {
      let org_list = answers.orgs.split(',');
      return (org_list.length >= 3) ?
        org_list[0] :
        "master";
    },
  },
  {
    "type": "input",
    "name": "SF_ORG__QA__URL",
    "message": "What is the URL of your QA Org?",
    when: function (answers) {
      return answers.orgs.includes('QA');
    },
    validate: storeSecret('SF_ORG__QA__SERVERURL'),
    default: 'https://test.salesforce.com'
  },
  {
    "type": "input",
    "name": "SF_ORG__QA__USERNAME",
    "message": "What is the username of your QA Org?",
    when: function (answers) {
      return answers.orgs.includes('QA');
    },
    validate: storeSecret('SF_ORG__QA__USERNAME')
  },
  {
    "type": "password",
    "name": "SF_ORG__QA__PASSWORD",
    "message": "Please enter the password and security token for the QA org",
    when: function (answers) {
      return answers.orgs.includes('QA');
    },
    validate: storeSecret('SF_ORG__QA__PASSWORD')
  },
  {
    "type": "input",
    "name": "SF_ORG__UAT__URL",
    "message": "What is the URL of your UAT Org?",
    when: function (answers) {
      return answers.orgs.includes('UAT');
    },
    validate: storeSecret('SF_ORG__UAT__SERVERURL'),
    default: 'https://test.salesforce.com'
  },
  {
    "type": "input",
    "name": "SF_ORG__UAT__USERNAME",
    "message": "What is the username of your UAT Org?",
    when: function (answers) {
      return answers.orgs.includes('UAT');
    },
    validate: storeSecret('SF_ORG__UAT__USERNAME')
  },
  {
    "type": "password",
    "name": "SF_ORG__UAT__PASSWORD",
    "message": "Please enter the password and security token for the UAT org?",
    when: function (answers) {
      return answers.orgs.includes('UAT');
    },
    validate: storeSecret('SF_ORG__UAT__PASSWORD')
  },
  {
    "type": "input",
    "name": "SF_ORG__SIT__URL",
    "message": "What is the URL of your SIT Org?",
    when: function (answers) {
      return answers.orgs.includes('SIT');
    },
    validate: storeSecret('SF_ORG__SIT__SERVERURL'),
    default: 'https://test.salesforce.com'
  },
  {
    "type": "input",
    "name": "SF_ORG__SIT__USERNAME",
    "message": "What is the username of your SIT Org?",
    when: function (answers) {
      return answers.orgs.includes('SIT');
    },
    validate: storeSecret('SF_ORG__SIT__USERNAME')
  },
  {
    "type": "password",
    "name": "SF_ORG__SIT__PASSWORD",
    "message": "Please enter the password and security token for the SIT org?",
    when: function (answers) {
      return answers.orgs.includes('SIT');
    },
    validate: storeSecret('SF_ORG__SIT__PASSWORD')
  },
  {
    "type": "input",
    "name": "SF_ORG__PROD__URL",
    "message": "What is the URL of your PROD Org?",
    when: function (answers) {
      return answers.orgs.includes('PROD');
    },
    validate: storeSecret('SF_ORG__PROD__SERVERURL'),
    default: 'https://login.salesforce.com'
  },
  {
    "type": "input",
    "name": "SF_ORG__PROD__USERNAME",
    "message": "What is the username of your PROD Org?",
    when: function (answers) {
      return answers.orgs.includes('PROD');
    },
    validate: storeSecret('SF_ORG__PROD__USERNAME')
  },
  {
    "type": "password",
    "name": "SF_ORG__PROD__PASSWORD",
    "message": "Please enter the password and security token for the PROD org?",
    when: function (answers) {
      return answers.orgs.includes('PROD');
    },
    validate: storeSecret('SF_ORG__PROD__PASSWORD')
  },
  {
    "type": "password",
    "name": "ADX_REFRESH_TOKEN",
    "message": "Please provide the Amplify DX Refresh Token?",
    when: answers => {
      return answers.continuousIntegrationType === 'gitlab';
    },
    validate: function (input, answers) {
      if (input == '') {
        return 'You must provide the Amplify DX Refresh Token';
      }
      const secretFunc = storeSecret('ADX_REFRESH_TOKEN');
      return secretFunc(input, answers);
    },
  }
  ],
  icing: [{
    description: 'Running Yarn Install so we can do some fancy stuff for you',
    cmd: ['yarn', 'install'],
    cmdOptions: {
      shell: true,
      cwd: '.ezbake/scripts'
    }
  },
  {
    description: 'Creating a CI Configuration file',
    cmd: [`<% if(continuousIntegrationType !== 'none') { return 'node' } else { return 'echo' } %>`,
      `<% if(continuousIntegrationType !== 'none') { return '.ezbake/scripts/ezbaker.js' } else { return '  . Not Required' } %>`
    ],
    cmdOptions: {
      shell: true,
    }
  },
  {
    description: 'Writing secret variables to local .env file and/or to GitLab CI',
    cmd: ['node', '.ezbake/scripts/secrets.js'],
    cmdOptions: {
      shell: true,
    }
  },
  ]
}
