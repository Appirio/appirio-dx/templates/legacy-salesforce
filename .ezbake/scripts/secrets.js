const fs = require('fs');
const path = require('path');
const child_process = require('child_process');
const rimraf = require('rimraf');
const os = require('os');
const colors = require('ansi-colors');
const config = require('@appirio/appirio/lib/config/config');
const ci_tasks = require('@appirio/appirio/lib/ci/ci-tasks');
const { processError } = require('@appirio/appirio/lib/util/misc');

const cwd = path.resolve(process.cwd());
const pathToEnvFile = path.join(cwd, '.env');
const pathToSecrets = path.join(cwd, '.ezbake');
let targetRepo = '<%= gitOriginURL %>';
const failedCISecrets = {};

// Method to convert the multi-line values into a quoted and single line value
const formatValue = value => {
  if (!value || typeof value !== 'string') {
    value = '';
  }
  let finalValue = value;
  const test = '(\r\n|\n|\r)';
  const re = new RegExp(test, 'gm');
  const match = value.match(re);
  if (match) {
    finalValue = '"' + value.replace(re, '\\n') + '"';
  }
  return finalValue;
};

//Method to write .env file
function createEnvFile(secrets) {
  let contents = Object.keys(secrets)
    .sort()
    .map(secretName => {
      secrets[secretName] = formatValue(secrets[secretName]);
      return `${secretName} = ${secrets[secretName]}` + os.EOL;
    })
    .reduce((previous, current) => {
      return previous.concat(current);
    }, '');

  fs.writeFileSync(pathToEnvFile, contents, {
    encoding: 'utf8'
  });
  console.log(`. Wrote ${pathToEnvFile} successfully`);
}

const writeCiConfig = async ({ envFile, key, body }) => {
  if (targetRepo) {
    try {
      let f, k, b;
      if (envFile) {
        f = envFile;
        console.log(`. Writing secrets from file ${envFile} to CI`);
      } else {
        k = key;
        b = body;
        console.log(`\n  . Writing secret ${key} to CI`);
      }
      await ci_tasks.writeProjectSecrets(f, k, b);
    } catch (err) {
      const errMsg = processError(err).message;
      if (key) {
        failedCISecrets[key] = body || '';
        console.log(`! ${colors.yellow(`Failed to write secret ${key} to CI.`)}`);
      } else {
        console.log(`! ${colors.yellow(`Failed to write secrets from file ${envFile} to CI.`)}`);
      }
      console.log(`! ${colors.yellow(`Error: ${errMsg}`)}`);
    }
  }
  return true;
};

let warningMessages = [];
if ( <%= continuousIntegrationType !== 'none' %>) {
  if (!config.hasSecret('sonarqube.access_token')) {
    warningMessages.push(`SonarQube access token was not found.
    After creating the SonarQube access token, store it in SONAR_LOGIN secret variable in Gitlab for your project.`);
  }

  if ( <%= usesCMC %> && !config.hasSecret('cmc.refresh_token')) {
    warningMessages.push(`CMC Refresh Token not found.
  CI Jobs and commands that interact with CMC will fail unless it's set.
  After setting it, store it in CMC_REFRESH_TOKEN secret variable on GitLab for any existing projects.`);
  }
} else {
  warningMessages.push(`CI not enabled during project creation.
  Please refer the below page once you enable a CI system for this project.
  https://dx.appirio.com/docs/project-setup/ci-setup/`);
}

const secrets = <%= JSON.stringify(_secrets) %>;
const notSoSecret = {
  SF_DEPLOY__ENABLED: 'false',
  SF_DEPLOY__TEST_SUITES: '',
  SF_ORG__DEPLOY__TEST_LEVEL: 'NoTestRun',
  SF_ORG__VERSION: '48.0',
};

Promise.resolve()
  .then(async () => {
    if (targetRepo && <%= continuousIntegrationType !== 'none' %>) {
      process.env.GIT_TOKEN = '<%= ci_cd__personal_token %>';
      await writeCiConfig({
        key: 'GITLAB_USERNAME',
        body: '<%= repo__username %>' || config.readUserConfig('gitlab.username')
      });
      await writeCiConfig({
        key: 'GITLAB_TOKEN',
        body: '<%= repo__personal_token %>' || config.getSecret('gitlab.personal_token')
      });

      // If using CMC in the project, write refresh token to the CI system
      if ( <%= usesCMC %> && config.hasSecret('cmc.refresh_token')) {
        await writeCiConfig({
          key: 'CMC_REFRESH_TOKEN',
          body: config.getSecret('cmc.refresh_token')
        });
      }

      // Write additional CI secrets to the CI system that should not be in .env
      if ( <%= enableSonarQube %> && config.hasSecret('sonarqube.access_token')) {
        await writeCiConfig({
          key: 'SONAR_LOGIN',
          body: config.getSecret('sonarqube.access_token')
        });
      }

      // Write all of the CI secrets from the secrets object to the CI system, without writing them to the .env file
      for (secretName in secrets) {
        await writeCiConfig({
          key: secretName,
          body: secrets[secretName]
        });
      }

      // Create a temporary .env file
      createEnvFile(notSoSecret);

      // Write all of the CI secrets from the temporary .env file to the CI system
      if (fs.existsSync(pathToEnvFile)) {
        await writeCiConfig({
          envFile: pathToEnvFile
        });
      }

      // If any secrets were not written to CI due to an error, those secrets will be written to the .env file and
      // show a warning message towards at the end of the script
      if (Object.keys(failedCISecrets).length !== 0 && failedCISecrets.constructor === Object) {
        const allLocal = {
          ...failedCISecrets,
          ...notSoSecret
        }
        createEnvFile(allLocal);
        warningMessages.push(`We were not able to successfully send your credentials to GitLab.
      Correct your GitLab permissions and run ${colors.underline('adx ci:secret -f .env')} to configure GitLab with these credentials.
      After doing this, you can delete/empty the .env file.`);
      }
    } else {
      // If CI information is not available, secrets will be written to the .env file
      const allLocal = {
        ...secrets,
        ...notSoSecret
      };
      createEnvFile(allLocal);
    }

    console.log('\n  . Deleting Setup Files');
    rimraf(pathToSecrets, (err) => {
      if (err) {
        warningMessages.push('Failed to delete .ezbake folder!');
      }
      if (warningMessages.length) {
        console.log(colors.yellow('WARNING(S):'));
        warningMessages.forEach((warningMessage, index) => {
          console.log(colors.yellow(`\n  ${index + 1}: ${warningMessage}`));
        });
        console.log(colors.yellow('\n  Use the Amplify DX Desktop App to set any missing configurations.'));
      }
    });
  });