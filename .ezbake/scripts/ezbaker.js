const { write } = require('@appirio/appirio/lib/util/yaml');
const gulpLog = require('fancy-log');

const {
  execSync
} = require('child_process');

var orgName = '<%= orgs %>'; //QA,SIT,UAT,PROD //<%= orgs %>
var orgs = orgName.split(','); //[ 'QA', 'SIT', 'UAT', 'PROD' ]

console.log(`  Orgs selected: ${orgs}`);

//Static jobs
let gitlabConfig = {
  image: "appirio/dx:latest",
  stages: [
    "deploy",
    "validate"
  ]
};

if (<%= enableSonarQube %>) { //true //<%= enableSonarQube %>
  const sonarJobs = {
    sonarqube_scan: {
      stage: "quality_scan",
      script: [
        "sonar-scanner-ee -Dsonar.login=$SONAR_LOGIN -Dsonar.qualitygate.wait=true -Dsonar.qualitygate.timeout=300"
      ],
      except: [
        "tags",
        "schedules"
      ]
    },
    sonarqube_merge_request_scan: {
      stage: "quality_scan",
      script: [
        "git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME",
        "sonar-scanner-ee -Dsonar.login=$SONAR_LOGIN -Dsonar.qualitygate.wait=true -Dsonar.qualitygate.timeout=300"
      ],
      only: [
        "merge_requests"
      ],
    }
  };

  gitlabConfig = Object.assign({}, gitlabConfig, sonarJobs);
  gitlabConfig.stages.unshift('quality_scan');
}

if (<%= usesCMC %>) {
  const cmcManifestCreationJob = {
    cmc_manifest_creation: {
      stage: "track_changes",
      script: [
        "adx cmc:manifest --message \"$CI_COMMIT_REF_NAME $CI_COMMIT_MESSAGE\""
      ],
      allow_failure: true,
      only: [
        "/^feature\\/.*/"
      ],
      except: [
        "tags",
        "schedules"
      ]
    }
  };

  gitlabConfig = Object.assign({}, gitlabConfig, cmcManifestCreationJob);
  gitlabConfig.stages.unshift('track_changes');
}

if (<%= cleanUpBranches %>) { //true // <%= cleanUpBranches %>
  const cleanUpJob = {
    cleanup: {
      stage: "cleanup",
      variables: {
        GIT_STRATEGY: "clone"
      },
      script: [
        "adx ci:shell --script cleanUp.sh --arguments master QA SIT"
      ],
      only: [
        "schedules"
      ]
    }
  };

  gitlabConfig = Object.assign({}, gitlabConfig, cleanUpJob);
  gitlabConfig.stages.unshift('cleanup');
}

//Common code for dynamic jobs
const commonCodeJSON = {
  cache: {
    key: "$CI_COMMIT_REF_SLUG",
    paths: [
      "node"
    ]
  },
  except: [
    "schedules"
  ]
};


validateJobJSON = {};
deployJobJSON = {};

const execCommand = () => {
  spawn();
}

// Make an initial Git commit so we can tag it
execSync(`git add .gitignore && git commit -m "Initial commit"`);
if (orgs.length >= 3) {
  execSync(`git branch ${orgs[0]}`);
}
if (orgs.length >= 4) {
  execSync(`git branch ${orgs[1]}`);
}
let tagName = '';
for (var i = 0; i < orgs.length; i++) {
  // Then add 'target-start' tags for each org
  tagName = `${orgs[i].toLowerCase()}-start`;
  console.log(`. Adding git tag ${tagName}`);
  execSync(`git tag -a ${tagName} -m "Execute deployments to the ${orgs[i]} org based on the difference from this point"`);

  validationKey = 'validate_against_' + orgs[i];
  deployKey = 'deploy_to_' + orgs[i];

  //Validation jobs
  validateJobJSON[validationKey] = {
    stage: "validate",
    script: [
      "adx sort:check"
    ],
  };
  //Deployment jobs
  deployJobJSON[deployKey] = {
    stage: "deploy",
    script: [
      "adx sort:check"
    ],
  };

  //Merge objects so that they have common code
  validateJobJSON[validationKey] = Object.assign({}, validateJobJSON[validationKey], commonCodeJSON);
  deployJobJSON[deployKey] = Object.assign({}, deployJobJSON[deployKey], commonCodeJSON);

  //Dynamically write out jobs based on which orgs user chose.
  validateJobJSON[validationKey].script.push("adx deploy:package --deploy.testLevel NoTestRun --target " + orgs[i]);
  deployJobJSON[deployKey].script.push("adx deploy:package --deploy.testLevel RunLocalTests --deploy.checkOnly false --target " + orgs[i]);

  //Initialize only array with blank value
  deployJobJSON[deployKey].only = [];
  validateJobJSON[validationKey].only = [];

  if (orgs[i] != 'UAT' && orgs[i] != 'PROD') {
    deployJobJSON[deployKey].only.push("/^" + orgs[i] + "/");
  }

  if (orgs[i] === 'QA') {
    validateJobJSON[validationKey].only.push("/^feature\/.*/");
  }

  if (orgs[i] === 'SIT') {
    if (!orgs.includes('QA'))
      validateJobJSON[validationKey].only.push("/^feature\/.*/");
    else
      validateJobJSON[validationKey].only.push("/^QA/");
  }

  if (orgs[i] === 'UAT') {
    if (orgs.includes('SIT')) {
      validateJobJSON[validationKey].only.push("/^SIT/");
    } else if (orgs.includes('QA')) {
      validateJobJSON[validationKey].only.push("/^QA/");
    } else
      validateJobJSON[validationKey].only.push("/^feature\/.*/");
    deployJobJSON[deployKey].only.push("master");
  }

  if (orgs[i] === 'PROD') {
    if (orgs.includes('QA') || orgs.includes('SIT') || orgs.includes('UAT'))
      validateJobJSON[validationKey].only.push("master");
    else
      validateJobJSON[validationKey].only.push("/^feature\/.*/");
    deployJobJSON[deployKey].only.push("/^v[0-9.]+$/");
    deployJobJSON[deployKey].when = "manual";
  }

  gitlabConfig[validationKey] = validateJobJSON[validationKey];
  gitlabConfig[deployKey] = deployJobJSON[deployKey];
}

const writeGitLabCiYaml = () => {
  const gitlabYMLFile = '.gitlab-ci.yml';
  const optionsForYml = { lineWidth: -1 };
  write(gitlabYMLFile, gitlabConfig, optionsForYml, (err) => {
    if (err != null) {
      gulpLog(err);
    }
  });
};

writeGitLabCiYaml();
